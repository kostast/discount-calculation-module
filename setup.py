# coding=utf-8

from setuptools import setup

setup(
    name='vinted_homework',
    version='0.1',
    packages=['discounter','tests',],
    py_modules=['application'],
    entry_points={
        'console_scripts': [
            'vdiscount = application:main',
        ]
    },
    url='',
    license='',
    author='Kostas Tamošiūnas',
    author_email='tamosiunas@gmail.com',
    description='homework_150525'
)
