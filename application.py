# coding=utf-8

import argparse
import sys
from decimal import Decimal
import decimal
from discounter.discount_engine import Discounter
from discounter.exceptions import ParcelsCouldNotBeLoaded


def get_parser():
    parser = argparse.ArgumentParser(description='Shipment discount '
                                                 'calculation.')
    parser.add_argument('--input_file', default='input.txt',
                        help='File containing list of transactions')
    parser.add_argument('--json_parcels', default='prices.json',
                        help='Parcel prices file')
    parser.add_argument('--monthly_limit', default='10',
                        help='Monthly discount limit')
    return parser

def main():
    try:
        args = get_parser().parse_args()
        discounter = Discounter(args.input_file,
                                args.json_parcels,
                                Decimal(args.monthly_limit))
        sys.stdout.write(discounter.execute())
    except FileNotFoundError as e:
        sys.stderr.write("ERROR: File '%s' not found\n" % e.filename)
    except ParcelsCouldNotBeLoaded as e:
        sys.stderr.write("Error: %s\n" % e)
    except decimal.InvalidOperation as e:
        sys.stderr.write("Error: Invalid decimal value for monthly limit\n")

if __name__ == '__main__':
    main()