# coding=utf-8

"""
Classes which are capable of reading transaction files.
"""

import datetime
from discounter.transaction import RawTransaction


class TransactionReader(object):
    """Reads transaction file. yields transactions."""

    def __init__(self, lines):
        self.lines = lines

    def transactions(self):
        """Returns generator of transaction items."""

        for line in self.lines:
            yield self.parse_transaction(line)

    @staticmethod
    def parse_transaction(raw_line):
        """Parse transaction line to transaction object."""

        line = raw_line.strip()
        transaction = RawTransaction(raw_entry=line)
        components = raw_line.split()
        try:
            date_str = components[0]
            transaction_date = datetime.datetime.strptime(date_str,
                                                          '%Y-%m-%d').date()
            transaction.date = transaction_date
        except (IndexError, ValueError):
            pass

        if len(components) > 1:
            transaction.size = components[1]

        if len(components) > 2:
            transaction.shipment_provider = components[2]

        return transaction

    @classmethod
    def from_file(cls, file_name, encoding='utf-8'):
        """Creates TransactionReader object with data loaded from file."""

        with open(file_name, 'r', encoding=encoding) as data_file:
            return cls(data_file.readlines())
