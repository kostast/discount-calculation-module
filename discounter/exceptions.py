# coding=utf-8

"""
Discounter application exceptions
"""


class DuplicateParcelsException(Exception):
    """
    Raised when ParcelTable gets 2 parcels with same shipment_provider,
    package_size and currency but different shipment_price.
    """

    def __init__(self, current_parcel, new_parcel):
        self.current_parcel = current_parcel
        self.new_parcel = new_parcel
        msg = "Adding %s parcel while %s already exists" % (
            self.new_parcel, self.current_parcel)
        super().__init__(msg)


class AbnormalTransactionSituation(Exception):
    """General exception class indicating issues in transactions."""


class NegativePriceException(AbnormalTransactionSituation):
    """Raised when negative price was set in TransactionWithDiscount."""

class NegativeDiscountException(AbnormalTransactionSituation):
    """Raised when negative discount was set in TransactionWithDiscount."""
    pass


class ParcelsCouldNotBeLoaded(Exception):
    """General exception indicating failure while loading parcels."""
    pass
