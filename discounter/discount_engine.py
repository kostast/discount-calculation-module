# coding=utf-8

"""
Here should be defined modules orchestrating discount calculation.
"""

from discounter.discounters import MonthlyDiscountLimiter, \
    SmallPackageDiscounter, NThtMonthlyDiscounter
from discounter.parcel import ParcelTable
from discounter.parcel_readers import JSONParcelReader
from discounter.transaction import TransactionWithDiscount
from discounter.transaction_readers import TransactionReader
import io


class Discounter:
    """Organises discount calculation as defined in exercise."""

    def __init__(self, transactions_file, prices_file, monthly_limit):
        self.transactions_file = transactions_file
        self.prices_file = prices_file
        if monthly_limit < 0:
            raise ValueError("Monthly limit can not be negative.")
        if not monthly_limit.is_finite():
            raise ValueError("Monthly limit has to be finite number")
        self.monthly_limit = monthly_limit
        self.parcel_table = None
        self.transactions = []
        self.transactions_with_discounts = []

    # pylint: disable=no-self-use
    def _get_parcel_reader(self):
        """Returns parcel reader. Extension point."""
        return JSONParcelReader

    # pylint: disable=no-self-use
    def get_transactions_reader(self):
        """Returns transaction reader. Extension point."""
        return TransactionReader

    # pylint: disable=no-self-use
    def get_currency(self):
        """Returns default currency. Extension point."""
        return "€"

    # pylint: disable=no-self-use
    def get_decimal_formatter(self):
        """Returns formatters for decimals. Extension point."""
        return "%.2f"

    def load_parcel_table(self):
        """Reads Parcel pricing and puts it to local ParcelTable."""

        parcel_reader_cls = self._get_parcel_reader()
        reader = parcel_reader_cls.from_file(self.prices_file)
        parcel_table = ParcelTable()
        for parcel in reader.parcels():
            parcel_table.add(parcel)
        self.parcel_table = parcel_table

    def load_transactions(self):
        """Loads transactions to transaction list."""

        transaction_reader_cls = self.get_transactions_reader()
        reader = transaction_reader_cls.from_file(self.transactions_file)
        self.transactions = list(reader.transactions())

    def get_default_parcel(self, transaction):
        """
        Finds default parcel for given transaction. Returns
        TransactionWithDiscount object.
        """

        discounted_transaction = TransactionWithDiscount.\
            from_raw_transaction(transaction)
        if not discounted_transaction.main_fields_set:
            # Could not determine parcel.
            # Lets give item back without modification
            return discounted_transaction
        try:
            parcel = self.parcel_table.get(
                discounted_transaction.shipment_provider,
                discounted_transaction.size,
                self.get_currency())
            discounted_transaction.parcel = parcel
        except KeyError:
            pass
        return discounted_transaction

    def get_default_parcels(self):
        """Finds default parcel for each transaction object."""

        for transaction in self.transactions:
            discounted_transaction = self.get_default_parcel(transaction)
            self.transactions_with_discounts.append(discounted_transaction)

    def get_discounts(self):
        """
        Actual discount calculation engine.
        It creates discounter objects and feeds to them transactions.
        Discounter objects suggest final price and discount.
        MonthlyDiscountLimiter will accept or reject discount.
        Important: Transactions needs to be in chronological order.
        """
        monthly_limiter = MonthlyDiscountLimiter(self.monthly_limit)
        small_package_discounter = SmallPackageDiscounter(self.parcel_table)
        monthly_discounter = NThtMonthlyDiscounter(self.parcel_table)
        discounters = [small_package_discounter, monthly_discounter]

        for transaction in self.transactions_with_discounts:
            for discounter in discounters:
                suggestion = discounter.suggest(transaction)
                if suggestion:
                    discount = monthly_limiter.max_discount(
                        transaction.date, suggestion.discount)
                    transaction.apply_discount(discount)

    def generate_output(self):
        """Returns string with generated output"""

        output = io.StringIO()

        # Prepare formatters
        price_discount_formatter = "%s " + self.get_decimal_formatter() + \
                                   " " + self.get_decimal_formatter()
        price_none_formatter = "%s " + self.get_decimal_formatter() + " -"

        # Format output
        for transaction in self.transactions_with_discounts:
            if transaction.main_fields_set and transaction.parcel:
                if transaction.discount:
                    line = price_discount_formatter % (transaction.raw_entry,
                                                       transaction.price,
                                                       transaction.discount)
                else:
                    line = price_none_formatter % (transaction.raw_entry,
                                                   transaction.price)
            else:
                line = "%s Ignored" % transaction.raw_entry
            output.write(line)
            output.write("\n")
        rez = output.getvalue()
        output.close()
        return rez

    def execute(self):
        """Shortcut method for data loading and discount calculation."""

        self.load_parcel_table()
        self.load_transactions()
        self.get_default_parcels()
        self.get_discounts()
        return self.generate_output()
