# coding=utf-8


"""
Classes for storing transaction information. Separate classes used for raw and
processed transactions.
"""

import logging
import sys
from discounter.exceptions import AbnormalTransactionSituation, NegativeDiscountException, \
    NegativePriceException

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


# pylint: disable=too-few-public-methods
class RawTransaction:
    """
    Defines transaction object which refer to transaction from input file.
    """

    def __init__(self, raw_entry, date=None, size=None, shipment_provider=None):
        self.logger = logging.getLogger()
        self.raw_entry = raw_entry
        self.date = date
        self.size = size
        self.shipment_provider = shipment_provider

    @property
    def main_fields_set(self):
        """
        Returns True when date, size and shipment_provider are set (not
        necessary with valid values).
        """
        return bool(self.date and self.size and self.shipment_provider)

    def __eq__(self, other):
        return self.raw_entry == other.raw_entry and \
            self.date == other.date and \
            self.size == other.size and \
            self.shipment_provider == other.shipment_provider

    def __repr__(self):
        return "%s(%s, date=%s, size=%s, shipment_provider=%s)" % (
            self.__class__.__name__,
            self.raw_entry,
            self.date,
            self.size,
            self.shipment_provider
        )


# pylint: disable=too-many-arguments
class TransactionWithDiscount(RawTransaction):
    """
    Transaction object holding extra fields: parcel, price and discount.
    """

    def __init__(self, raw_entry, date=None, size=None, shipment_provider=None,
                 parcel=None, price=None, discount=None):
        super().__init__(raw_entry, date, size, shipment_provider)
        self._parcel = None
        self._price = None
        self._discount = None
        self.parcel = parcel
        if price is not None:
            self.price = price
        self.discount = discount

    @property
    def parcel(self):
        """Returns parcel that was set."""
        return self._parcel

    @parcel.setter
    def parcel(self, parcel):
        """
        Sets parcel. If parcel is not None - sets new price by taking parcel
        price.
        """

        self._parcel = parcel
        if parcel is not None:  # Setting default price when parcel is changing
            self.price = parcel.shipment_price

        # Probably parcel should not be changed once defined or price was set.
        if parcel is None and self.price is not None:
            self.logger.warn("Setting new parcel value '%s' for object '%s' "
                             "when price is defined", parcel, self)

    @property
    def price(self):
        """Returns transaction price."""

        return self._price

    @price.setter
    def price(self, new_price):
        """
        Sets new price. Raises NegativePriceException when negative price set.
        """

        if new_price is not None and new_price < 0:
            raise NegativePriceException("New price '%s'for object %s is "
                                         "negative" % (new_price, self))
        if new_price is None:
            self.logger.warn("New price was set to None for %s", self)
        self._price = new_price

    @property
    def discount(self):
        """Returns discount."""
        return self._discount

    @discount.setter
    def discount(self, new_discount):
        """
        Sets discount value. Raises NegativeDiscountException when new_discount
        value is negative.
        """

        if new_discount is not None and self.price is None:
            self.logger.warn("Setting discount for %s when price is "
                             "unknown", self)
        if new_discount is not None and new_discount < 0:
            raise NegativeDiscountException(
                "Setting negative discount '%s' for %s", new_discount, self)
        self._discount = new_discount

    @classmethod
    def from_raw_transaction(cls, raw_transaction):
        """
        Creates new TransactionWithDiscount object from RawTransaction object.
        """

        return cls(raw_transaction.raw_entry,
                   date=raw_transaction.date,
                   size=raw_transaction.size,
                   shipment_provider=raw_transaction.shipment_provider)

    def apply_discount(self, discount):
        """
        Discounts transaction. Substracts from price and adds to discount.
        Raises exception AbnormalTransactionSituation when price is not set.
        """

        if self.price is None:
            raise AbnormalTransactionSituation("Could not apply discount for "
                                               "'%s'. Price is unknown." % self)
        self.price = self.price - discount
        if self.discount is None:
            self.discount = discount
        else:
            self.discount += discount

    def __eq__(self, other):
        return self.raw_entry == other.raw_entry and \
            self.date == other.date and \
            self.size == other.size and \
            self.shipment_provider == other.shipment_provider and \
            self.parcel == other.parcel and \
            self.price == other.price and \
            self.discount == other.discount

    def __repr__(self):
        return "%s(%s, date=%s, size=%s, shipment_provider=%s, parcel=%s, " \
               "price=%s, discount=%s)" % (self.__class__.__name__,
                                           self.raw_entry,
                                           self.date,
                                           self.size,
                                           self.shipment_provider,
                                           self.parcel,
                                           self.price,
                                           self.discount)

