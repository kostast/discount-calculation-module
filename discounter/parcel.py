# coding=utf-8

"""
Defines containers and data structures for storing and searching shipping
pricing.
"""

import decimal
from discounter.exceptions import DuplicateParcelsException


class Parcel:
    """
    Defines shipment provider, package size, shipment price and currency.
    """

    __slots__ = ('_shipment_provider',
                 '_package_size',
                 '_shipment_price',
                 '_currency')

    def __init__(self, shipment_provider, package_size, shipment_price,
                 currency):
        self._shipment_provider = shipment_provider
        self._package_size = package_size
        if isinstance(shipment_price, float):
            raise ValueError('Shipment price can not be float.')
        self._shipment_price = decimal.Decimal(shipment_price)
        if not self._shipment_price.is_finite():
            raise ValueError('Shipment price can be only finite number.')
        if self._shipment_price.is_zero():
            self._shipment_price = decimal.Decimal(0)
        self._currency = currency.upper()

    @property
    def shipment_provider(self):
        """Returns shipment_provider (currier)."""
        return self._shipment_provider

    @property
    def package_size(self):
        """Returns package size."""
        return self._package_size

    @property
    def shipment_price(self):
        """Returns shipment price."""
        return self._shipment_price

    @property
    def currency(self):
        """Returns currency."""
        return self._currency

    def __eq__(self, other):
        return self.shipment_provider == other.shipment_provider and \
            self.package_size == other.package_size and \
            self.shipment_price == other.shipment_price and \
            self.currency == other.currency

    def __repr__(self):
        return "%s(shipment_provider=%s, package_size=%s, shipment_price=%s, " \
               "currency=%s)" % (
                   self.__class__.__name__, self.shipment_provider,
                   self.package_size, self.shipment_price, self.currency)

    def __hash__(self):
        return hash((self.shipment_provider, self.package_size,
                     self.shipment_price, self.currency))


class ParcelTable:
    """
    Allows to store parcel information and retrieve it by indexed lookups or
    by searching attributes of stored items.
    """

    def __init__(self):
        self._items_list = []
        self._items_dict = {}

    @staticmethod
    def _get_index_key(parcel):
        """Generates dictionary lookup key from parcel."""

        return (parcel.shipment_provider,
                parcel.package_size,
                parcel.currency)

    def add(self, parcel):
        """Adds parcel to search tables. May raise DuplicateParcelsException."""

        indexing_key = self._get_index_key(parcel)
        if indexing_key in self._items_dict:
            existing_parcel = self._items_dict[indexing_key]
            raise DuplicateParcelsException(existing_parcel, parcel)
        self._items_dict[indexing_key] = parcel
        self._items_list.append(parcel)

    @property
    def items(self):
        """Returns all items in the table."""
        return list(self._items_list)

    def get(self, shipment_provider, package_size, currency):
        """Returns single item matching parameters or will raise KeyError."""

        indexing_key = (shipment_provider, package_size, currency.upper())
        return self._items_dict[indexing_key]

    def filter(self, params):
        """
        Filters table by parameters. Parameter are given as dictionary, where
        keys are parcel attributes and values - expected parcel attribute
        values. Parcels need to match all parameters to be given in result.
        Returns list with parcel. Returns empty list if no matching parcels was
        found.
        """
        if 'currency' in params:
            params['currency'] = params['currency'].upper()
        results = []
        for item in self._items_list:
            match_filter = True
            for param_key, param_val in params.items():
                match_attribute = getattr(item, param_key) == param_val
                if not match_attribute:
                    match_filter = False
                    break
            if match_filter:
                results.append(item)
        return results
