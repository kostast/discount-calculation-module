# coding=utf-8

"""
Package containing classes responsible for suggesting and calculating discounts.

Generally classes which suggest discount will take TransactionWithDiscount
object, calculate discount and return DiscountSuggestion object or None.
TransactionWithDiscount object should be not modified.
"""

from collections import namedtuple
import logging
import sys

from decimal import Decimal

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

DiscountSuggestion = namedtuple('DiscountSuggestion', ['price', 'discount'])

# pylint: disable=too-few-public-methods
class MonthlyDiscountLimiter:
    """
    Calculates if this month discount limit was not overused. Informs if
    discounts still can be applied.
    """

    def __init__(self, monthly_limit):
        self._monthly_limit = monthly_limit
        self._used = 0
        self._last_month = None

    def _check_month_changed(self, date):
        """
        Check if month or year has changed. If yes - reset monthly discount
        limits.
        """

        year_month = date.year, date.month
        if self._last_month is None:
            self._last_month = year_month
        if self._last_month != year_month:
            self._used = Decimal(0)
            self._last_month = year_month

    def max_discount(self, date, amount):
        """Returns max discount that can be applied."""

        self._check_month_changed(date)
        available_limit = self._monthly_limit - self._used
        current_discount = min(available_limit, amount)
        self._used += current_discount
        return current_discount


class SmallPackageDiscounter:
    """Suggest price by the cheapest parcel from the category."""

    def __init__(self, parcel_table, package_size='S', currency="€"):
        self.logger = logging.getLogger()
        self.parcel_table = parcel_table
        self._currency = currency
        self._package_size = package_size
        self._get_cheapest_package_price()

    def _get_cheapest_package_price(self):
        """Gets price of cheapest package meeting parameters."""
        parcels = self.parcel_table.filter(
            {'package_size': self._package_size,
             'currency': self._currency})
        prices = [parcel.shipment_price for parcel in parcels]
        if len(prices) == 0:
            self.logger.warning(
                "Could not determine prices for %s size packets in currency %s."
                " Default price 0", self._package_size, self._currency)
            self.min_parcel_price = None
        else:
            self.min_parcel_price = min(prices)

    def suggest(self, p_transaction):
        """
        p_transaction - Transaction like object with parcel attribute.
        returns DiscountSuggestion object or None if no suggestion was made.
        """
        if self.min_parcel_price is None:
            return
        if not p_transaction.parcel:
            return
        if p_transaction.parcel.package_size == self._package_size:
            discount = p_transaction.parcel.shipment_price - \
                       self.min_parcel_price
            return DiscountSuggestion(price=self.min_parcel_price,
                                      discount=discount)


class NThtMonthlyDiscounter:
    """
    Discounter which suggest to give one shipping for free.
    Size and provider - attributes to search.
    nth - which shipment will be given for free.
    """

    def __init__(self, parcel_table, size='L', provider='LP', nth=3):
        self.parcel_table = parcel_table
        self._current_month = None
        self._counter = 0
        if nth <= 0:
            raise ValueError("nth has to be greater or equal 1")
        self.nth = nth
        self.size = size
        self.provider = provider

    def _check_month_changed(self, date):
        """Resets counter if month or year has changed."""
        year_month = date.year, date.month
        if self._current_month is None:
            self._current_month = year_month
        if self._current_month != year_month:
            self._counter = 0
            self._current_month = year_month

    def suggest(self, p_transaction):
        """
        returns DiscountSuggestion object or None if no suggestion was made.
        """
        if not p_transaction.date or not p_transaction.parcel:
            return
        self._check_month_changed(p_transaction.date)

        if p_transaction.parcel.package_size == self.size and \
            p_transaction.parcel.shipment_provider == self.provider:
            self._counter += 1
            if self._counter == self.nth:
                return DiscountSuggestion(
                    price=0,
                    discount=p_transaction.parcel.shipment_price)
