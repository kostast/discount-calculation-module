# coding=utf-8

"""
Readers for shipping pricing information.
"""

from decimal import Decimal
import json
from discounter.exceptions import ParcelsCouldNotBeLoaded
from discounter.parcel import Parcel


class JSONParcelReader(object):
    """
    Takes iterable (e.g. list) with package sending prices and returns parcel
    generator.
    """

    def __init__(self, prices):
        self.prices = prices

    @staticmethod
    def parse_price(price_dict):
        """ Parses price_dict object and returns Parcel object

        :param price_dict: '{"provider": "MR",
                             "package_size": "S",
                             "price": "2",
                             "currency": "€"}'
        :return: Parcel object
        """
        return Parcel(shipment_provider=price_dict['provider'],
                      package_size=price_dict['package_size'],
                      shipment_price=Decimal(price_dict['price']),
                      currency=price_dict['currency'])

    def parcels(self):
        """Parses items and returns Parcel generator."""
        for price in self.prices:
            yield self.parse_price(price)

    @classmethod
    def from_file(cls, file_name, encoding='utf-8'):
        """Creates JSONParcelReader. Reads data from provided file."""
        with open(file_name, 'r', encoding=encoding) as data_file:
            try:
                json_price_list = json.load(data_file)
                return cls(json_price_list)
            except ValueError:
                raise ParcelsCouldNotBeLoaded("Seems that '%s' is not valid "
                                              "json file" % file_name)
