# coding=utf-8

import datetime
import logging
import unittest
from decimal import Decimal
from discounter.discounters import MonthlyDiscountLimiter, \
    SmallPackageDiscounter, NThtMonthlyDiscounter
from discounter.parcel import Parcel, ParcelTable
from discounter.transaction import TransactionWithDiscount


class MonthlyDiscountLimiterTestCase(unittest.TestCase):

    def setUp(self):
        self.discounter = MonthlyDiscountLimiter(10)

    def test_getting_discount(self):
        rez = self.discounter.max_discount(datetime.date(2015, 1, 2), 2)
        self.assertEqual(rez, 2)

    def test_overusing_discount(self):
        self.discounter.max_discount(datetime.date(2015, 1, 2), 2)
        rez = self.discounter.max_discount(datetime.date(2015, 1, 2), 9)
        self.assertEqual(rez, 8)

    def test_month_changes(self):
        self.discounter.max_discount(datetime.date(2015, 1, 2), 2)
        rez = self.discounter.max_discount(datetime.date(2015, 2, 2), 9)
        self.assertEqual(rez, 9)


class SmallPackageDiscounterTestCase(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def tearDown(self):
        logging.disable(logging.NOTSET)

    def _generate_parcel_table(self):
        parcel_leur = Parcel('Provider', 'Large', Decimal(10), 'Eur')
        parcel_seur = Parcel('Provider', 'Small', Decimal(11), 'Eur')
        parcel_lusd = Parcel('Provider', 'Large', Decimal(10), 'Usd')
        parcel_ousd = Parcel('Other', 'Large', Decimal(10), 'Usd')
        table = ParcelTable()
        table.add(parcel_leur)
        table.add(parcel_seur)
        table.add(parcel_lusd)
        table.add(parcel_ousd)
        return table

    def test_finding_min_parcel_price(self):
        table = self._generate_parcel_table()
        discounter = SmallPackageDiscounter(table, 'Large', 'Eur')
        self.assertEqual(discounter.min_parcel_price, 10)

    def _generate_transaction(self):
        parcel = Parcel('Provider', 'Large', 15, 'Eur')
        return TransactionWithDiscount("2001-02-03 S LP",
                                       datetime.date(2001, 2, 3),
                                       'Large',
                                       'LP',
                                       parcel, 15, None)

    def test_finding_min_parcel_price_with_empty_table(self):
        table = ParcelTable()
        discounter = SmallPackageDiscounter(table, 'Large', 'Eur')
        self.assertEqual(discounter.min_parcel_price, None)

        transaction = self._generate_transaction()
        self.assertIsNone(discounter.suggest(transaction))

    def test_finding_normal_discount(self):
        table = self._generate_parcel_table()
        transaction = self._generate_transaction()

        discounter = SmallPackageDiscounter(table, 'Large', 'Eur')
        suggestion = discounter.suggest(transaction)
        self.assertIsNotNone(suggestion)

        self.assertEqual(suggestion.price, 10)
        self.assertEqual(suggestion.discount, 5)

    def test_discount_when_parcel_not_set(self):
        table = self._generate_parcel_table()
        transaction = TransactionWithDiscount("2001-02-03 S LP",
                                              datetime.date(2001, 2, 3),
                                              'Large')
        discounter = SmallPackageDiscounter(table, 'Large', 'Eur')
        suggestion = discounter.suggest(transaction)
        self.assertIsNone(suggestion)


class NThtMonthlyDiscounterTestCase(unittest.TestCase):

    def setUp(self):
        self.parcel_large = Parcel('Provider', 'Large', 15, 'Eur')
        self.parcel_small = Parcel('Provider', 'Small', 10, 'Eur')

    def _generate_parcel_table(self):
        parcel_leur = Parcel('Provider', 'Large', Decimal(10), 'Eur')
        parcel_seur = Parcel('Provider', 'Small', Decimal(11), 'Eur')
        parcel_lusd = Parcel('Provider', 'Large', Decimal(10), 'Usd')
        parcel_ousd = Parcel('Other', 'Large', Decimal(10), 'Usd')
        table = ParcelTable()
        table.add(parcel_leur)
        table.add(parcel_seur)
        table.add(parcel_lusd)
        table.add(parcel_ousd)
        return table

    def test_regular_discounting(self):
        table = self._generate_parcel_table()
        transaction = TransactionWithDiscount("2001-02-03 S LP",
                                              datetime.date(2001, 2, 3),
                                              'Large',
                                              'Provider',
                                              self.parcel_large, 15, None)
        discounter = NThtMonthlyDiscounter(table, 'Large', 'Provider')
        suggestion1 = discounter.suggest(transaction)
        suggestion2 = discounter.suggest(transaction)
        suggestion3 = discounter.suggest(transaction)
        suggestion4 = discounter.suggest(transaction)

        self.assertIsNone(suggestion1)
        self.assertIsNone(suggestion2)
        self.assertIsNone(suggestion4)

        self.assertIsNotNone(suggestion3)
        self.assertEqual(suggestion3.price, 0)
        self.assertEqual(suggestion3.discount, 15)

    def test_month_changes(self):
        table = self._generate_parcel_table()
        transaction1 = TransactionWithDiscount("2001-02-03 S LP",
                                               datetime.date(2001, 2, 3),
                                               'Large',
                                               'Provider',
                                               self.parcel_large, 15, None)
        transaction2 = TransactionWithDiscount("2001-03-03 S LP",
                                               datetime.date(2001, 3, 3),
                                               'Large',
                                               'Provider',
                                               self.parcel_large, 15, None)
        discounter = NThtMonthlyDiscounter(table, 'Large', 'Provider')
        suggestion1 = discounter.suggest(transaction1)
        suggestion2 = discounter.suggest(transaction1)

        suggestion3 = discounter.suggest(transaction2)
        suggestion4 = discounter.suggest(transaction2)
        suggestion5 = discounter.suggest(transaction2)
        suggestion6 = discounter.suggest(transaction2)

        self.assertIsNone(suggestion1)
        self.assertIsNone(suggestion2)
        self.assertIsNone(suggestion3)
        self.assertIsNone(suggestion4)
        self.assertIsNotNone(suggestion5)
        self.assertIsNone(suggestion6)

        self.assertIsNotNone(suggestion5)
        self.assertEqual(suggestion5.price, 0)
        self.assertEqual(suggestion5.discount, 15)


    def test_bad_transaction_object(self):
        table = self._generate_parcel_table()
        transaction = TransactionWithDiscount("A")
        discounter = NThtMonthlyDiscounter(table, 'Large', 'Provider', nth=1)
        suggestion = discounter.suggest(transaction)
        self.assertIsNone(suggestion)

    def test_nth_less_than_1(self):
        table = self._generate_parcel_table()
        self.assertRaises(ValueError, NThtMonthlyDiscounter, table, 'Large',
                          'Provider', nth=0)

if __name__ == '__main__':
    unittest.main()
