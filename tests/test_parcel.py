# coding=utf-8

import unittest
from discounter.exceptions import DuplicateParcelsException
from discounter.parcel import Parcel, ParcelTable
from decimal import Decimal


class ParcelTestCase(unittest.TestCase):
    def setUp(self):
        self.parcel = Parcel('Provider', 'Large', Decimal(10), 'Eur')

    def test_parcel_currency_case_sensitivity(self):
        """Currency should be case insensitive"""
        new_parcel = Parcel('Provider', 'Large', Decimal(10), 'eur')
        self.assertEqual(self.parcel, new_parcel,
                         "Currency should not be case sensitive")

    def test_price_must_be_convertible_to_decimal(self):
        """
        Shipment price has to be Decimal. Arguments can be objects that are
        convertible to Decimal. Except float.
        """
        parcel_price_int = Parcel('Provider', 'Large', 10, 'Eur')
        parcel_price_dec = Parcel('Provider', 'Large', Decimal('10.0'), 'Eur')
        parcel_price_str = Parcel('Provider', 'Large', '10.0', 'Eur')

        self.assertEqual(self.parcel, parcel_price_int)
        self.assertEqual(self.parcel, parcel_price_dec)
        self.assertEqual(self.parcel, parcel_price_str)

    def test_shipment_price_cant_be_float(self):
        """Float brings bad design patterns. Lets avoid them."""
        self.assertRaises(ValueError, Parcel, 'Provider', 'Large', 10.0, 'Eur')

    def test_shipment_price_must_be_a_number(self):
        """Infinity and NaN a not valid prices."""

        self.assertRaises(ValueError, Parcel, 'Provider', 'Large', 'NaN', 'Eur')
        self.assertRaises(ValueError, Parcel, 'Provider', 'Large', 'Infinity',
                          'Eur')

    def test_shipment_price_will_not_be_negative_zero(self):
        """Shipment price should not be negative zero. It may confuse later."""

        parcel = Parcel('Provider', 'Large', Decimal('-0'), 'Eur')
        self.assertFalse(parcel.shipment_price.is_signed())

    def test_shipment_price_might_be_negative(self):
        """Shipment price might be negative for some strange reason."""

        parcel = Parcel('Provider', 'Large', Decimal('-1'), 'Eur')
        self.assertTrue(parcel.shipment_price.is_signed())


class ParcelTableTestCase(unittest.TestCase):

    def setUp(self):
        self.parcel_leur = Parcel('Provider', 'Large', Decimal(10), 'Eur')
        self.parcel_seur = Parcel('Provider', 'Small', Decimal(11), 'Eur')
        self.parcel_lusd = Parcel('Provider', 'Large', Decimal(10), 'Usd')
        self.parcel_ousd = Parcel('Other', 'Large', Decimal(10), 'Usd')
        self.table = ParcelTable()
        self.table.add(self.parcel_leur)
        self.table.add(self.parcel_seur)
        self.table.add(self.parcel_lusd)

    def test_empty_parcel_table(self):
        table = ParcelTable()
        self.assertListEqual(table.items, [])

    def test_add_parcel(self):
        parcel = Parcel('Provider', 'Large', Decimal(10), 'Eur')
        table = ParcelTable()
        table.add(parcel)
        self.assertListEqual(table.items, [parcel])

    def test_add_duplicate_parcel(self):
        """
        We don't allow to add multiple parcels from same provider that are
        same size and currency.
        """
        parcel1 = Parcel('Provider', 'Large', Decimal(10), 'Eur')
        parcel2 = Parcel('Provider', 'Large', Decimal(11), 'Eur')
        table = ParcelTable()
        table.add(parcel1)
        self.assertRaises(DuplicateParcelsException, table.add, parcel2)

    def test_get_parcel_by_attributes(self):
        small_eur = self.table.get('Provider', 'Small', 'Eur')
        large_eur = self.table.get('Provider', 'Large', 'Eur')
        large_usd = self.table.get('Provider', 'Large', 'Usd')
        self.assertEqual(small_eur, self.parcel_seur)
        self.assertEqual(large_eur, self.parcel_leur)
        self.assertEqual(large_usd, self.parcel_lusd)

    def test_get_not_existing_parcel(self):
        self.assertRaises(KeyError, self.table.get, 'Not', 'Existing', 'Parcel')

    def test_parcel_filters_single_attribute(self):
        eur_filter1 = self.table.filter({'currency': 'Eur'})
        eur_filter2 = self.table.filter({'currency': 'EUR'})
        self.assertEqual(eur_filter1, eur_filter2)

        self.assertEqual(set(eur_filter1),
                         set([self.parcel_leur, self.parcel_seur]))

    def test_parcel_filters_multiple_attributes(self):
        filtered = self.table.filter({'package_size': 'Large',
                                      'shipment_provider': 'Provider'})
        self.assertEqual(set(filtered),
                         set([self.parcel_leur, self.parcel_lusd]))


if __name__ == '__main__':
    unittest.main()
