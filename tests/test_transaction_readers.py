# coding=utf-8

import datetime
import os
import unittest
from discounter.transaction import RawTransaction
from discounter.transaction_readers import TransactionReader


class TransactionReaderTestCase(unittest.TestCase):

    def test_parse_one_line(self):
        data = ["2015-02-17 S MR\n"]
        reader = TransactionReader(data)
        t = RawTransaction("2015-02-17 S MR",
                           datetime.date(2015, 2, 17), 'S', 'MR')
        self.assertListEqual(list(reader.transactions()), [t])

    def test_parse_multiple_lines(self):
        data = ["2015-02-17 S MR\n",
                "2015-02-18 L LP\n"]
        reader = TransactionReader(data)
        t1 = RawTransaction("2015-02-17 S MR",
                            datetime.date(2015, 2, 17), 'S', 'MR')
        t2 = RawTransaction("2015-02-18 L LP",
                            datetime.date(2015, 2, 18), 'L', 'LP')
        self.assertListEqual(list(reader.transactions()), [t1, t2])

    def test_bad_details(self):
        """Bad ate and record structure."""
        data = ["2015-02-29 CUSPS\n"]
        reader = TransactionReader(data)
        t = RawTransaction("2015-02-29 CUSPS", None, 'CUSPS', None)

        self.assertListEqual(list(reader.transactions()), [t])

    def test_bad_date(self):
        data = ["2015-02-30 S MR\n"]
        reader = TransactionReader(data)
        t = RawTransaction("2015-02-30 S MR", None, 'S', "MR")
        self.assertListEqual(list(reader.transactions()), [t])

    def test_fully_corrupted_record(self):
        data = ["CUPS\n"]
        reader = TransactionReader(data)
        t = RawTransaction("CUPS", None, None, None)
        self.assertListEqual(list(reader.transactions()), [t])

    def get_fixtures_file(self, filename):
        directory = os.path.dirname(os.path.abspath(__file__))
        return os.path.join(directory, 'fixtures', filename)

    def test_reading_file(self):
        file_name = self.get_fixtures_file('raw_transactions.txt')
        reader = TransactionReader.from_file(file_name)
        transactions = list(reader.transactions())
        self.assertEqual(len(transactions), 4)
        self.assertTrue(transactions[0].main_fields_set)
        self.assertFalse(transactions[2].main_fields_set)


if __name__ == '__main__':
    unittest.main()
