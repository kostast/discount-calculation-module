# coding=utf-8

import os
import unittest
from decimal import Decimal
from discounter.discount_engine import Discounter



class DiscountEngineTestCase(unittest.TestCase):

    def get_fixtures_file(self, filename):
        directory = os.path.dirname(os.path.abspath(__file__))
        return os.path.join(directory, 'fixtures', filename)

    def test_sample(self):
        transactions_file = self.get_fixtures_file('demo_input.txt')
        prices_file = self.get_fixtures_file('parcels.json')
        discounter = Discounter(transactions_file, prices_file, Decimal(10))
        output = discounter.execute()
        expected_output_file = self.get_fixtures_file('demo_output.txt')
        with open(expected_output_file) as f:
            expected_output = f.read()
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()
