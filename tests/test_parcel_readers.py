# coding=utf-8

import os
import unittest
from decimal import Decimal
from discounter.exceptions import ParcelsCouldNotBeLoaded
from discounter.parcel_readers import JSONParcelReader


class JSONParcelReaderTestCase(unittest.TestCase):

    def test_empty_data_set(self):
        reader = JSONParcelReader([])
        parcels = list(reader.parcels())
        self.assertListEqual(parcels, [])

    def test_single_element(self):
        data = [{"provider": "MR",
                 "package_size": "S",
                 "price": "2",
                 "currency": "€"}]
        reader = JSONParcelReader(data)
        parcels = list(reader.parcels())
        self.assertEqual(len(parcels), 1)

        parcel = parcels[0]
        self.assertEqual(parcel.shipment_provider, "MR")
        self.assertEqual(parcel.package_size, "S")
        self.assertEqual(parcel.shipment_price, Decimal(2))
        self.assertEqual(parcel.currency, "€")

    def test_multiple_elements(self):
        data = [{"provider": "MR",
                 "package_size": "S",
                 "price": "2",
                 "currency": "€"},
                {"provider": "MR",
                 "package_size": "S",
                 "price": "2",
                 "currency": "€"}]
        reader = JSONParcelReader(data)
        parcels = list(reader.parcels())
        self.assertEqual(len(parcels), 2)

    def get_fixtures_file(self, filename):
        directory = os.path.dirname(os.path.abspath(__file__))
        return os.path.join(directory, 'fixtures', filename)

    def test_loading_single_entry_file(self):
        file_name = self.get_fixtures_file('parcel.json')
        reader = JSONParcelReader.from_file(file_name)
        parcels = list(reader.parcels())
        self.assertEqual(len(parcels), 1)

    def test_loading_multiple_entries_file(self):
        file_name = self.get_fixtures_file('parcels.json')
        reader = JSONParcelReader.from_file(file_name)
        parcels = list(reader.parcels())
        self.assertEqual(len(parcels), 6)

    def test_raising_parcels_could_not_be_loaded(self):
        file_name = self.get_fixtures_file('raw_transactions.txt')
        self.assertRaises(ParcelsCouldNotBeLoaded,
                          JSONParcelReader.from_file, file_name)

if __name__ == '__main__':
    unittest.main()
