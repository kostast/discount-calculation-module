# coding=utf-8

import unittest
from discounter.exceptions import AbnormalTransactionSituation, NegativeDiscountException
from discounter.parcel import Parcel
from discounter.transaction import TransactionWithDiscount, RawTransaction


class TransactionWithDiscountTestCase(unittest.TestCase):

    def setUp(self):
        self.transaction = TransactionWithDiscount("2001-02-03 S LP")

    def test_set_price(self):
        self.transaction.price = 5
        self.assertEqual(self.transaction.price, 5)

    def test_set_negative_price(self):
        with self.assertRaises(AbnormalTransactionSituation):
            self.transaction.price = -5
        self.assertEqual(self.transaction.price, None)

    def test_discount(self):
        self.transaction.price = 10
        self.transaction.discount = 5
        self.assertEqual(self.transaction.discount, 5)

    def test_negative_discount(self):
        with self.assertRaises(NegativeDiscountException):
            self.transaction.price = 10
            self.transaction.discount = -5

    def test_apply_discount(self):
        self.transaction.price = 10
        self.transaction.apply_discount(4)
        self.assertEqual(self.transaction.price, 6)
        self.assertEqual(self.transaction.discount, 4)

    def test_apply_negative_discount(self):
        self.transaction.price = 10
        with self.assertRaises(NegativeDiscountException):
            self.transaction.apply_discount(-4)

    def test_apply_discount_bigger_than_price(self):
        self.transaction.price = 10
        with self.assertRaises(AbnormalTransactionSituation):
            self.transaction.apply_discount(15)

    def test_apply_discount_without_price(self):
        with self.assertRaises(AbnormalTransactionSituation):
            self.transaction.apply_discount(15)

    def test_apply_multiple_discounts(self):
        self.transaction.price = 10
        self.transaction.apply_discount(4)
        self.transaction.apply_discount(5)
        self.assertEqual(self.transaction.price, 1)
        self.assertEqual(self.transaction.discount, 9)

    def test_object_equality(self):
        p1 = Parcel('Provider', 'Large', 10, 'Eur')
        p2 = Parcel('Provider', 'Large', 12, 'Eur')
        o1 = TransactionWithDiscount("2001-02-03 S LP", 1, 2, 3, p1, 5, 6)
        o2 = TransactionWithDiscount("2001-02-03 S LP", 1, 2, 3, p1, 5, 6)
        o3 = TransactionWithDiscount("2001-02-03 S LP", 1, 2, 3, p2, 5, 6)
        self.assertEqual(o1, o2)
        self.assertNotEqual(o1, o3)

    def test_creation_from_raw_transaction_object(self):
        raw_transaction = RawTransaction("2001-02-03 S LP", 1, 2, 3)
        t_discounts = TransactionWithDiscount.from_raw_transaction(
            raw_transaction)
        self.assertEqual(t_discounts.raw_entry, "2001-02-03 S LP")
        self.assertEqual(t_discounts.date, 1)
        self.assertEqual(t_discounts.size, 2)
        self.assertEqual(t_discounts.shipment_provider, 3)
