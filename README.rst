Shipment discount calculation module
====================================

Description and requirements
https://gist.github.com/raimondasv/4dce3ec02ff1e1568361

Installation/Deployment
=======================

Running application and tests
-----------------------------
Solution is made having python3.3+ in mind. (Recommended 3.4)

Launching application manual way form source folder:

``python3.4 application.py``

Or by using command line parameters:

``python3.4 application.py --input_file some_input.txt --json_parcel prices.json --monthly_limit 8.3``


Launching tests:

``python3.4 -m unittest discover``


Launching coverage and pylint (manually)
----------------------------------------
Firstly we should load python3.3+ environment (for firstimers it might be tricky) and then execute following commands::

  pip install coverage pylint
  coverage run -m unittest discover
  pylint discounter

Automatic testing
-----------------
There is tool which can manage environments during testing::

  pip install tox
  tox -l # lists environments
  tox -e py33 #will launch testing and coverage on python 3.3
  tox # will execute testing on python 3.3 and 3.4. Will launch lint tools on python 3.4.

Do not forget to install corresponding python versions.

Installing
----------
If your default python version 3.3+ you can use::

  python setup.py install

Now ``vdiscount`` command should be available in your shell.


Design solution
===============
Program can be split to multiple parts:

* data containers
* data loaders
* discount suggesters
* discount engine

Data containers
---------------
Stores information. Supports minimal amount of actions. E.g. Parcel class - stores information about carrier, shipment size and price (information from transaction file). Similar are transaction classes.

To this category also belongs ParcelTable class (responsible for information lookup)

Data loaders
------------
Reads data sources and yields data containers (in our case reads files, but they made to be extendable/swapable). Samples: TransactionReader, JSONParcelReader.

Discount suggesters
-------------------
Takes transaction or transactions (in our case only one transaction) and gives suggestion of shipping price and discount. Verdict if price has to be accepted made by discount engine. In this solution discount suggesters works in streaming mode with assumption that data is given in chronological order.

Discount engine
---------------
Loads data, indexes, makes data mappings. Makes verdict on each item final shipping price and total discount. Generates output.

General
-------

Most components supports steaming (python generators).

Components designed to be extensible and swappable. Some of them has extension
points. Trying to keep balance between flexibility, extensibility and sane
defaults (avoiding over engineering).

